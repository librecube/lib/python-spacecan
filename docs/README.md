# Python SpaceCAN Documentation

This is the documentation for the Python implementation of SpaceCAN.
Please read the [SpaceCAN specification](https://librecube.gitlab.io/standards/spacecan)
to understand the concepts of this robust onboard communication protocol.

This implementation provides the fundamentals to set up and work with SpaceCAN
as underlying communication protocol. A higher level application layer, such
as an onboard software application, interfaces with this implementation through
two ways: by calling the class methods to trigger actions (this is mostly done
the controller node), and by function overriding to process received reports and data.

Consider for example the switch to the redundant bus channel. The controller
node simply calls:

```python
controller.switch_bus()
```

On the responder side, once the responder detecs the missing hearbeats and
switches to the other bus channel, you can implement (override) the relevant
stub function:

```python
def on_bus_switch():
    print("Hey there, the bus switched. Ohoh.")

responder.on_bus_switch = on_bus_switch
```

This is a common scheme and applies for most of the functionality.

## Setup

To create a SpaceCAN network you need one controller node and one or more
responder nodes. The controller always has node id 0, whereas the responders
can have ids from 1 to 127. Lower ids have higher priority on the bus.

## Creating Nodes

To create and start the controller node, do:

```python
import spacecan
controller = spacecan.Controller.from_file("config/controller.json")
controller.received_telemetry = received_telemetry
controller.connect()  # connect to the underlying CAN network
controller.start()  # start the node (threads for heartbeats, sync, etc)
```

> Note: Instead of using a file, you can pass the configuration to the
`Controller` class at instantiation. Using config files is however more convenient.

Similar for a responder node:
```python
import spacecan
responder = spacecan.Responder.from_file("config/responder1.json")
responder.connect()
responder.start()
```

Depending on your configuration, at least the heartbeat frames should now
be emitted from controller and received by the responders. You can see those
with a CAN sniffer.

## Configuration Files

The basic configuration, captured in a configuration file or passed as
arguments to the `Controller` consists of:

- `interface`: Physcial CAN interface to use, such as `socketcan`
- `channel_a`: Name of nominal channel, such as `can0`
- `channel_b`: Name of redundant channel, such as `can1`
- `heartbeat_period` (opt): Periodicity of the heartbeat frames in seconds (float)
- `sync_period` (opt): Periodicity of the sync frames in seconds (float)

If either `heartbeat_period` and `sync_period` are optional and their functionality
is deactived if they are not specified.

For the responder, the configuration consists of:

- `interface`: Physcial CAN interface to use, such as `socketcan`
- `channel_a`: Name of nominal channel, such as `can0`
- `channel_b`: Name of redundant channel, such as `can1`
- `node_id`: The node id of this responder (in the range 1-127)
- `heartbeat_period` (opt, float): Periodicity of the heartbeat frames in seconds
- `max_miss_heartbeat` (def: 3, int): Number of consecutive missed heartbeats to trigger a bus switch
- `max_bus_switch` (def: None, int): Number of maximum consecutive bus switches (`None` for indefinite)

As shown above, the node configuration can be loaded from a JSON file. It must
have following format:

```json
{
    "interface": "socketcan",
    "channel_a": "vcan0",
    "channel_b": "vcan1",
    "node_id": 1,
    "heartbeat_period": 0.5,
    "max_miss_heartbeat": 5
}
```

## Basic Functionalities

The basic functionalities of SpaceCAN consist of redundancy management, synchronization,
time distribution, and telecommand/telemetry.

### Redundancy Management

The controller can trigger the switch to the other bus channel with `controller.switch_bus()`.
The responders detect that no more heartbeats are on the current channel and
will switch to the other channel. A stub method is called when that happens `responder.on_bus_switch()`.

There is also a stub for sent and received heartbeats, namely `controller.sent_heartbeat()` and `responder.received_heartbeat()`, respectively. They can be used for heartbeat monitoring (eg. blink an LED).

### Synchronization

If so configured, the controller will send out periodic sync frames, for responder
nodes to carry out synchronized activities. The relevant stub is `responder.received_sync()`

### Time Distribution

The controller can send SCET and UTC time updates to responders. This is done as such:

```python
controller.send_scet(coarse_time, fine_time)
controller.send_utc(day, ms_of_day, sub_ms_of_day)
```

The responder needs to override the relevant stubs:

```python
def received_scet(coarse_time, fine_time):
    # do something with that information

def received_utc(day, ms_of_day, sub_ms):
    # do something with that information

responder.received_scet = received_scet
responder.received_utc = received_utc
```

### Telecommand and Telemetry

The controller can send a telecommand of up to 8 bytes to a responder. The
id of the receiving node needs to be provided:

```python
controller.send_telecommand(data, node_id)
```

> The `data` can be a bytearray or a list of 8-bit numbers.

The responder needs to implement the relevant stub to process this telecommand:

```python
def received_telecommand(data):
    # do something with that data

responder.received_telecommand = received_telecommand
```

Responder nodes can send telemetry of up to 8 bytes to the controller:

```python
responder.send_telemetry(data)
```

The controller needs to implemented the relevant stub to process the received
telemetry:

```python
def received_telemetry(data, node_id):
    # do something with that data from that node id

controller.received_telemetry = received_telemetry
```

It is up to you (ie. the upper layer application on the controller and responder
nodes) on how to handle/process the data transferred in the telecommand and telemetry
frames.

## Packet Protocol

To overcome the inherent limitation of maximum 8 bytes data payload of the CAN frames,
a packet protocol mechanism is available. This lets you send a maximum
of 256 * 6 bytes = 1536 bytes in one packet. Here again, it is up to you (ie. the upper
layer application) on how to interpret the data transferred in the packets.

To enable the packet protocol, you need to do so in the config file:

```json
{
    ...
    "packet_service": true
}
```

The controller and responder nodes send and receive packets in similar way:

```python
controller.send_packet(packet, node_id)
responder.send_packet(packet)
```

> The `packet` can be a bytearray or a list of 8-bit numbers.

The controller receives packets from the responders as such:

```python
def received_packet(packet_data, node_id):
    # do something with the received packet

controller.received_packet = received_packet
```

Similar for the responder nodes, but without the `node_id` argument.

## SpaceCAN Service Protocol

The SpaceCAN Service Protocol (SSP) uses the packet protocol and provides the concept of
services to a higher application layer. This service concept is largely inspired
by the ECSS Packet Utilization Service (PUS). However, whereas PUS is designed
for space-ground communication, the SSP is designed for onboard controller-responder
communication.

The following services are available:

- ST01 - Request Verification Service
- ST03 - Housekeeping Service
- ST08 - Function Management Service
- ST17 - Test Service
- ST20 - Parameter Service

Services requests and reports have the format: `service_type, service_subtype, [data]`.

For example, the connection test request is: `(17,1)` with no data. The associated
report is `(17,2)`.

### Service Configuration

Similar to the basic SpaceCAN configuration, the configuration of the services
is done via configuration files. Keep in mind that SPP uses packet protocol,
which needs to be enabled in the basic configuration file (see above section).

To configure the services for the controller do:

```python
controller = spacecan.Controller.from_file("config/controller.json")
services = spacecan.Services(controller)

services.from_file("config/services_responder1.json", node_id=1)
services.from_file("config/services_responder2.json", node_id=2)
```

As can be seen, the controller services are configured *per* responder. This
becomes evident because responders typically have different functionality
and reporting data.

Each responder is configured as following. For example for responder 1:

```python
responder = spacecan.Responder.from_file("config/responder1.json")
services = spacecan.Services(responder)

services.from_file("config/services_responder1.json")
```

The sections below specify what needs to be added to the configuration file
for each service, if applicable.

### ST01 - Request Verification Service

This service reports to the controller the status of the acceptance and execution
of a request sent to a responder. An acceptance report is generated immideatly
after completion of validity checks of the received request; an execution report
is generated after the execution of the request.

The controller thus can override following generic stub to receive all such reports:

```python
services.request_verification.received_report(node_id, case, source_packet)
```
where `case` is one of `(1,1)`, `(1,2)`, `(1,7)` or `(1,8)` and `source_packet` is the
originating service request type and subtype.

For example, when controller sends a connection test request `(17,1)`, the responder
first replies with a case `(1,1)` and source_packet `(17,1)` that request was
successfully accepted.

Alternatively, one can override the following specific stubs:

```python
services.request_verification.received_success_acceptance_report(node_id, source_packet)
services.request_verification.received_fail_acceptance_report(node_id, source_packet)
services.request_verification.received_success_completion_report(node_id, source_packet)
services.request_verification.received_fail_completion_report(node_id, source_packet)
```

The request verification service is useful to indicate to the controller
whether or not a request was received and executed successfully.

### ST03 - Housekeeping Service

The housekeeping service type provides the visibility of any on-board parameters
assembled in housekeeping reports. The structure of the reports used by the
housekeeping service are predefined in the configuration file in such way:

```json
{
    "housekeeping_reports": [
        {
            "report_id": 1,
            "comment": "includes full set of parameters",
            "interval": 10,
            "enabled": true,
            "parameter_ids": [1, 2, 3, 4]
        },
        {
            "report_id": 2,
            "interval": 5,
            "enabled": false,
            "parameter_ids": [3, 4]
        }
    ]
}
```

Each report gets a `report_id` and has a defined periodicity `interval` in seconds.
Reports starts either enabled or disabled, defined by the `enabled` flag.
The `parameter_ids` is a list of parameters that are contained in this report, in that order.
Parameters are defined in the `Parameter Management Service` (see below).

The controller can enable `(3,5)` and disable `(3,6)` such period housekeeping
reports or request a single shot report `(3,27)` from a responder:

```python
services.housekeeping.send_enable_period_housekeeping_reports(node_id, report_ids)
services.housekeeping.send_disable_period_housekeeping_reports(node_id, report_ids)
services.housekeeping.send_single_shot_housekeeping_reports(node_id, report_ids)
```

The `report_ids` is a list of one or more report ids.

The responder replies with housekeeping reports `(3,25)`, which can
be processed by the controller through overriding the relevant stub:

```python
def received_housekeeping_report(node_id, report, report_id):
    # do something with the report data

services.housekeeping.received_housekeeping_report = received_housekeeping_report
```

### ST08 - Function Management Service

This service allows the controller to request execution of functions `(8,1)` from
the responders. Functions are defined as follows:

```json
    "functions": [
        {
            "function_id": 1,
            "function_name": "reset counter",
            "comment": "sets the counter back to zero"
        },
        {
            "function_id": 2,
            "function_name": "set switch",
            "arguments": [
                {
                    "argument_id": 1,
                    "argument_name": "status",
                    "encoding": "B"
                }
            ]
        }
    ]
```

Each function has an id, a name, and optionally arguments. The functions are
implemented by the responder.

The controller can request execution of such function:

```python
services.function_management.send_perform_function(node_id, function_id, arguments)
```

The responder must implement such function by overriding the stub and implementing
the specific function code for each function id:

```python
def perform_function(function_id, arguments):
    if function_id == 1:
        # do something
    elif function_id == 2:
        # do something different

services.function_management.perform_function = perform_function      
```

### ST17 - Test Service

This is a simple ping service to check if a responder is active `(17,1)` or if an
application process (APID) of a responder is active `(17,3)`:

```python
services.test.send_connection_test(node_id)
services.test.send_application_connection_test(node_id, apid)
```

The responder responds with a `(17,2)` or `(17,4)` respectively, which
the controller can process by overriding relevant stubs:

```python
services.received_connection_test_report(node_id)
services.received_application_connection_test_report(node_id, apid)
```

### ST20 - Parameter Management Service

The parameter management service type provides capabilities to the controller
for reading values of on-board parameters from responders.

Parameters are defined as follows:

```json
{
    "parameters": [
        {
            "parameter_id": 1,
            "parameter_name": "counter",
            "comment": "a monotonic counter",
            "encoding": "i",
            "value": 0
        },
        {
            "parameter_id": 2,
            "parameter_name": "switch status",
            "encoding": "B",
            "value": 0
        },
        {
            "parameter_id": 3,
            "parameter_name": "temperature",
            "encoding": "f",
            "value": 0
        }
    ]
}
```

Parameters are given an id, a name, a starting value, and an encoding. Possible encodings are:

| Format | C Type | Python Type | Size |
| - | -| - |- |
| b | signed char | integer | 1 |
| B | unsigned char | integer | 1 |
| h | short | integer | 2 |
| H | unsigned short | integer | 2 |
| l | long | integer | 4 |
| L | unsigned long | integer | 4 |
| f | float | float | 4 |
| d | double | float | 8 |

The controller can request a report `(20,1)` of the values one or more parameters:

```python
services.parameter_management.send_report_parameter_values(node_id, parameter_ids)
```

The responder replies with such a report `(20,2)`, which can be processed by the controller
through overriding this stub:

```python
def received_parameter_value_report(node_id, report):
    # do something with the report data

services.parameter_management.received_parameter_value_report = received_parameter_value_report
```
