# Examples

Here you can find examples of how to set up a SpaceCAN network. The examples
use the virtual CAN network that is available in Linux. If you use Windows or Mac
you will need to find out if similar feature exist.

There are three examples that you can try out. Each example consists of a
controller node and two responder nodes (with id 1 and 2). Of course you can add more
nodes easily.

- **basic**: This example shows the basic SpaceCAN functionalities, including telemetry and telecommand frames (max. 8 bytes)
- **packet**: This example demonstrates the sending of variable length data as packets.
- **services**: This example shows how to use the SpaceCAN Service Protocol (SSP).

## Set up virtual CAN network

You will need to set up the virtual CAN network:

```bash
sudo ip link add dev vcan0 type vcan
sudo ip link add dev vcan1 type vcan
sudo ip link set up vcan0
sudo ip link set up vcan1
```

For convenience these commands are put in a file that you can run:

```bash
source vcan.sh
```

You will need to have sudo permissions to bring up the network.

## Run the examples

Each example is started in the same way.

First start the controller:

```bash
python controller.py
```

And then in separate shells start the responders:

```bash
python responder1.py
```

```bash
python responder2.py
```

## Install can-utils for Linux (optional)

If you want to monitor the traffic on the virtual CAN networks, install `can-utils`
of your Linux distribution. To monitor the traffic on vcan0 run:

```bash
candump vcan0
```
