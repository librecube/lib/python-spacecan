import time
import spacecan


def received_heartbeat():
    print("heartbeat received")


def on_bus_switch():
    print("bus switched")


def received_packet(packet_data):
    print(f"packet received [{packet_data.hex()}]")


responder = spacecan.Responder.from_file("config/responder1.json")
# responder.received_heartbeat = received_heartbeat
responder.on_bus_switch = on_bus_switch
responder.received_packet = received_packet
responder.connect()
responder.start()

print("Running...")
try:
    while True:
        time.sleep(10)
        # send dummy packet
        data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
        packet = spacecan.Packet(data)
        responder.send_packet(packet)

except KeyboardInterrupt:
    pass
except Exception as e:
    print("Error:", e)

responder.stop()
responder.disconnect()
print("Stopped")
