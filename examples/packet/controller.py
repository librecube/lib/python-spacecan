import time
import spacecan


def received_packet(data, node_id):
    print(f"packet received [{data.hex()}] from node {node_id}")


controller = spacecan.Controller.from_file("config/controller.json")
controller.received_packet = received_packet
controller.connect()
controller.start()

INTRO_TEXT = """
    <Stop program with Ctrl-C>

    Enter the following, where x is destination node id:
        px,<hexdata>: Send variable lengh data packet to node x
        Example: p1,00112233 to send 0x00112233 to responder node 1

    Enter 'h' to show this text again.
"""
print(INTRO_TEXT)

try:
    while True:
        x = input("").lower()

        if x == "h":
            print(INTRO_TEXT)

        elif x.startswith("p"):
            node_id = int(x.split(",")[0][1:])
            data = bytearray.fromhex(x.split(",")[1])
            packet = spacecan.Packet(data)
            controller.send_packet(packet, node_id)

except KeyboardInterrupt:
    print()
except Exception as e:
    print("Error:", e)

controller.stop()
controller.disconnect()
