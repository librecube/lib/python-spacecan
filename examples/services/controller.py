import spacecan


# create controller node
controller = spacecan.Controller.from_file("config/controller.json")

# create spacecan service protocol
services = spacecan.Services(controller)

# initialize services for responder node 1
services.from_file("config/services.json", node_id=1)

# initialize services for responder node 2
# (for this example we are re-using the same config)
services.from_file("config/services.json", node_id=2)


def print_report(node_id, report, report_id=None):
    if report_id is not None:
        print(f"--> received housekeeping report {report_id} from node {node_id}:")
    else:
        print(f"--> received parameter value report from node {node_id}:")
    for parameter_id, value in report.items():
        parameter = services.parameter_management.get_parameter(parameter_id)
        print(f" => {parameter.parameter_name}: {value}")


def print_command_verification_report(node_id, case, source_packet):
    print(
        f"--> received command verification status {case} for source packet {source_packet} from node {node_id}"
    )


services.parameter_management.received_parameter_value_report = print_report
services.housekeeping.received_housekeeping_report = print_report
services.request_verification.received_report = print_command_verification_report
services.test.received_connection_test_report = lambda node_id: print(
    f"--> received connection test report from {node_id}",
)


controller.connect()
controller.start()


INTRO_TEXT = """
    <Stop program with Ctrl-C>

    Enter commands using this pattern:   
        <n>,<s>,<t>,<data>
    
    where:
        n = node id
        s = service
        t = subtype
        data are comma seperated byte values, optional

        Example: 2,17,1 sends a test command (17,1) to node 2

    Enter 'h' to show this text again.
"""
print(INTRO_TEXT)

try:
    while True:
        x = input("").lower()

        if x == "h":
            print(INTRO_TEXT)

        else:
            try:
                x = x.split(",")
                node_id = int(x[0])
                service = int(x[1])
                subtype = int(x[2])

            except ValueError as e:
                print("invalid input")
                continue

            case = (service, subtype)

            # enable periodic housekeeping reports
            if case == (3, 5):
                report_ids = [int(i) for i in x[3:]]
                services.housekeeping.send_enable_period_housekeeping_reports(
                    node_id, report_ids
                )

            # disable periodic housekeeping reports
            elif case == (3, 6):
                report_ids = [int(i) for i in x[3:]]
                services.housekeeping.send_disable_period_housekeeping_reports(
                    node_id, report_ids
                )

            # request single shot housekeeping reports
            elif case == (3, 27):
                report_ids = [int(i) for i in x[3:]]
                services.housekeeping.send_single_shot_housekeeping_reports(
                    node_id, report_ids
                )

            # perform a function
            elif case == (8, 1):
                function_id = int(x[3])
                arguments = [int(y) for y in x[4:]] if len(x) > 4 else None
                services.function_management.send_perform_function(
                    node_id, function_id, arguments
                )

            # connection test
            elif case == (17, 1):
                services.test.send_connection_test(node_id)

            # application connection test
            elif case == (17, 3):
                apid = int(x[3])
                services.test.send_application_connection_test(node_id, apid)

            # report parameter values
            elif case == (20, 1):
                parameter_ids = [int(i) for i in x[3:]]
                services.parameter_management.send_report_parameter_values(
                    node_id, parameter_ids
                )

except KeyboardInterrupt:
    print()
except Exception as e:
    print("Error:", e)

controller.stop()
controller.disconnect()
