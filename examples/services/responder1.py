import time
import random
import spacecan


# create responder node
responder = spacecan.Responder.from_file("config/responder1.json")

# create spacecan service protocol
services = spacecan.Services(responder)

# initialize spp
services.from_file("config/services.json")

# map parameter alias to parameter ids
PARAM_COUNTER = 1
PARAM_SWITCH = 2
PARAM_TEMPERATURE = 3
PARAM_VOLTAGE = 4

# map function alias to function ids
FUNC_RESET_COUNTER = 1
FUNC_SET_SWITCH = 2


def perform_function(function_id, arguments):
    if function_id == FUNC_RESET_COUNTER:
        set_param(PARAM_COUNTER, 0)
    elif function_id == FUNC_SET_SWITCH:
        set_param(PARAM_SWITCH, arguments[0])


services.function_management.perform_function = perform_function

# helper functions
set_param = services.parameter_management.set_parameter_value
get_param = services.parameter_management.get_parameter_value

responder.connect()
responder.start()

try:
    print("Running...")
    while True:
        # do some other stuff
        time.sleep(0.1)

        # update housekeeping data
        set_param(PARAM_COUNTER, get_param(PARAM_COUNTER) + 1)
        set_param(PARAM_TEMPERATURE, 25 + random.random())
        if get_param(PARAM_SWITCH):
            set_param(PARAM_VOLTAGE, 5 + random.random())
        else:
            set_param(PARAM_VOLTAGE, 0)

except KeyboardInterrupt:
    pass
except Exception as e:
    print("Error:", e)

responder.stop()
responder.disconnect()
print("Stopped")
