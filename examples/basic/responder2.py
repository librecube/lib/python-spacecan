import time
import random
import struct
import spacecan


def received_heartbeat():
    print("heartbeat received")


def received_sync():
    print("sync received")


def received_scet(coarse_time, fine_time):
    print(f"scet received [{coarse_time, fine_time}]")


def received_utc(day, ms_of_day, sub_ms):
    print(f"utc received [{day, ms_of_day, sub_ms}]")


def received_telecommand(frame_data):
    print(f"telecommand received [{frame_data.hex()}]")


def on_bus_switch():
    print("bus switched")


responder = spacecan.Responder.from_file("config/responder2.json")
# responder.received_heartbeat = received_heartbeat
# responder.received_sync = received_sync
responder.received_scet = received_scet
responder.received_utc = received_utc
responder.received_telecommand = received_telecommand
responder.on_bus_switch = on_bus_switch
responder.connect()
responder.start()

print("Running...")
try:
    while True:
        time.sleep(5)

        # send dummy telemetry
        clock = int(time.time())
        temperature = 25 + random.random()
        data = struct.pack("!if", clock, temperature)
        responder.send_telemetry(data)

except KeyboardInterrupt:
    pass
except Exception as e:
    print("Error:", e)

responder.stop()
responder.disconnect()
print("Stopped")
