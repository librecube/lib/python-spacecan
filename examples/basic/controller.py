import time
import spacecan


def received_telemetry(data, node_id):
    print(f"telemetry received [{data.hex()}] from node {node_id}")


controller = spacecan.Controller.from_file("config/controller.json")
controller.received_telemetry = received_telemetry
controller.connect()
controller.start()

INTRO_TEXT = """
    <Stop program with Ctrl-C>

    Enter one of following command where x is destination node id:
        b: bus switch
        s: send scet
        u: send utc
        tx: send dummy telecommand frame to node x     

    Enter 'h' to show this text again.
"""
print(INTRO_TEXT)

try:
    while True:
        x = input("").lower()

        if x == "h":
            print(INTRO_TEXT)

        elif x == "b":
            print("switch bus")
            controller.switch_bus()

        elif x == "s":
            time_elapsed = time.monotonic()
            coarse_time = int(time_elapsed)
            fine_time = int((time_elapsed - coarse_time) * 1000)
            controller.send_scet(coarse_time, fine_time)

        elif x == "u":
            time_now = time.time()
            day = int(time_now / 60 / 60 / 24)  # days since 1970-01-01
            ms_of_day = int((time_now - day) * 1000)
            sub_ms_of_day = 0  # not using it
            controller.send_utc(day, ms_of_day, sub_ms_of_day)

        elif x.startswith("t"):
            node_id = int(x[1:])
            data = [1, 2, 3, 4, 5, 6, 7, 8]  # maximum of 8 bytes can be send in a frame
            controller.send_telecommand(data, node_id)

except KeyboardInterrupt:
    print()
except Exception as e:
    print("Error:", e)

controller.stop()
controller.disconnect()
